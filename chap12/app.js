import { createServer } from 'http';
import { hostname } from 'os'
// import { cpus } from 'os';
// import cluster from 'cluster';
// import { once } from 'events';

// if (cluster.isMaster) {
//   const availableCpus = cpus();
//   console.log(`Clustering to ${availableCpus.length} processes`)
//   availableCpus.forEach(() => cluster.fork())
//   // cluster.on('exit', (worker, code) => {
//   //   if (code !== 0 && !worker.exitedAfterDisconnect) {
//   //     console.log(
//   //       `Worker ${worker.process.pid} crashed. ` +
//   //       'Starting a new worker'
//   //     );
//   //     cluster.fork();
//   //   }
//   // })

//   process.on('SIGUSR2', async () => {
//     const workers = Object.values(cluster.workers)
//     for (const worker of workers) {
//       console.log(`Stopping worker: ${worker.process.pid}`);
//       worker.disconnect();
//       await once(worker, 'exit')
//       if (!worker.exitedAfterDisconnect) continue;
//       const newWorker = cluster.fork()
//       await once(newWorker, 'listening')
//     }
//   })
// } else {
//   const { pid } = process;
//   const server = createServer((req,res)=> {
//     let i = 1e7;
//     while (i>0) { i-- }

//     console.log(`Handling request from ${pid}`);
//     res.end(`Hello from ${pid}\n`)
//     setTimeout(() => {
//       throw new Error('oopps')
//     }, Math.ceil(Math.random() * 3) * 1000)
//   })
//   server.listen(8080, () => console.log(`Started at ${pid}`));
// }

// const { pid } = process;
// const server = createServer((req,res)=> {
//   let i = 1e7;
//   while (i>0) { i-- }
//   console.log(`Handling request from ${pid}`);
//   res.end(`Hello from ${pid}\n`)
// })

// const port = Number.parseInt(
//   process.env.PORT || process.argv[2]
// ) || 8080

// server.listen(port, () => console.log(`Started at ${pid}`));

const version = 1
const server = createServer((req, res) => {
  let i=1e7; while (i>0) {i--}
  res.end(`Hello from ${hostname()} (v${version})`)
})

server.listen(8080);