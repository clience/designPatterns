import { fork } from 'child_process'

export class ProcessPool  {
  constructor(file, poolMax) {
    this.file = file
    this.poolMax = poolMax
    this.pool = []
    this.active = []
    this.waiting = []
  }

  acquire() {
    return new Promise((resolve, reject) => {
      // 풀에 사용할 준비가 된 프로세스가 있으면 active 목록에 추가 후 resolve() 전달하여 
			// 외부 Promise 이행
      let worker
      if (this.pool.length > 0) {
        worker = this.pool.pop()
        this.active.push(worker)
        return resolve(worker)
      }

      // pool에 사용 가능한 프로세스가 없고, 이미 실행 중인 프로세스가 최대 프로세스에 도달 시 대기
      // 나중에 사용하기 위해 외부 프라미스의 resolve(), reject() 콜백을 대기열에 추가
      if (this.active.length >= this.poolMax) {
        return this.waiting.push({resolve, reject})
      }

      // 아직 최대 실행 중인 프로세스에 도달하지 않은 경우 child_process.fork() 사용하여 새 프로세스 생성
      // 새로 시작된 프로세스 부터 ready 메세지 기다린다.
      worker = fork(this.file)
      worker.once('message', message => {
        if (message === 'ready') {
          this.active.push(worker)
          return resolve(worker)
        }
        worker.kill()
        reject(new Error('Improper process start'))
      })
      worker.once('exit', code => {
        console.log(`Worker exited with code ${code}`);
        this.active = this.active.filter(w => worker !== w)
        this.pool = this.pool.filter(w => worker !== w)
      })
    })
  }

  // 프로세스가 완료되면 pool에 다시 넣는 역할
  release(worker) {
    // 대기목록에 요청 있을 시 대기열 선두에 있는 resolve() 콜백에 전달, release 중인 작업자를 재할당
    if (this.waiting.length > 0) {
      const { resolve } = this.waiting.shift()
      return resolve(worker)
    }
    // 아니면 release 중인 작업자를 active 목록에서 제거하고 풀에 다시 넣는다
    this.active = this.active.filter(w =>  worker !== w)
    this.pool.push(worker)
  }
 }