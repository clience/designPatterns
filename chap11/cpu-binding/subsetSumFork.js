import { EventEmitter } from 'events'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { ProcessPool } from './processPool.js'

const __dirname = dirname(fileURLToPath(import.meta.url))
const workerFile = join(__dirname,
  'workers', 'subsetSumProcessWorker.js')
const workers = new ProcessPool(workerFile, 2)

export class SubsetSum extends EventEmitter {
  constructor (sum, set) {
    super()
    this.sum = sum
    this.set = set
  }

  async start () {
    // 풀에서 새로운 자식 프로세스 획득
    // 작업이 완료되면 즉시 작업자 핸들을 사용하여 작업 데이터와 함께 메세지를 자식 프로세스에 전달
    
    const worker = await workers.acquire()
    worker.send({ sum: this.sum, set: this.set })

    // onMessage 이벤트 리스너에서 end 메세지 수신 확인
    const onMessage = msg => {
      if (msg.event === 'end') {
        worker.removeListener('message', onMessage)
        workers.release(worker)
      }

      this.emit(msg.event, msg.data)
    }

    // 새로운 리스너 연결
    worker.on('message', onMessage)
  }
}
